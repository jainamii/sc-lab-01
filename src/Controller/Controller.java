package Controller;
import Model.CardMachine;
import Model.CashCard;



public class Controller {
	private CardMachine mac1;
	private CashCard card1;
	
	public Controller(CardMachine mac1,CashCard card1){
		this.mac1 = mac1;
		this.card1 = card1;
		
	}
	public void refills(int amount){
		mac1.refills(card1,amount);
	}
	public int getBalance(){
		return card1.getBalance();
	}
	
	public void buyfood(int amount){
	card1.pay(amount);
	
	
	}
}
