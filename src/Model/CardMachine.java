package Model;

public class CardMachine {

	public void refills(CashCard card,int amount){
		card.setBalance(amount);
	}
	 
			
	public int getId(CashCard card){
		return card.getId();
	}
	
	
	public int getBalance(CashCard card) {
		return card.getBalance();
	}
	
}
