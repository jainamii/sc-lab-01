package Model;
public class CashCard {
	private int id;
	private int balance;

	public CashCard(int id, int balance){
		this.id = id;
		this.balance = balance;
	}

	public void pay(int price){
		if	(balance>=price){
			balance=balance-price;
		}
	}

	public void setBalance(int amount){
		balance += amount;
	}


	public int getBalance() {
		return balance;
	}

	public int getId(){
		return id;
	}

}
