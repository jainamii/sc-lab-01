package Viewer;
import Controller.Controller;
import Controller.Gui;
import Model.CardMachine;
import Model.CashCard;



public class Main {
	public static void main(String[] args) {
		CashCard card1 = new CashCard(1111, 0);
		CardMachine mac1 = new CardMachine();

		Controller con = new Controller(mac1, card1);
		Gui Border = new Gui(con);
	
	}
}
